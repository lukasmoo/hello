import tensorflow as tf
from tensorflow import keras
import matplotlib.pyplot as plt
import numpy as np

def polynom(x):
  #y = 100*x+10*x**2+x**3+0.1*x**4+0.01*x**5
  y = x**2
  return y

train_set = {}

for x in range(-10, 10):
  y = polynom(x)
  train_set[x] = y

x,y = zip(*sorted(train_set.items()))
#plt.plot(x,y, linewidth = 0, marker='x')
#plt.show()

model = tf.keras.models.Sequential([
#  keras.layers.Dense(units=2, activation='relu'),
  keras.layers.Dense(units=10, activation='relu'),
  keras.layers.Dense(units=10, activation='sigmoid'),
#  keras.layers.Activation('sigmoid'),
#  keras.layers.Dense(units=10),
#  keras.layers.Activation('sigmoid'),
  keras.layers.Dense(units=1)
])

model.compile(optimizer='adam', loss='mean_squared_error')

xs = np.array(x, dtype=float)
ys = np.array(y, dtype=float)

history = model.fit(xs, ys, epochs=500)

print("------------------------------------")
print(train_set[9.0])
print(model.predict([9.0]))

#print(history.history)
x_train,y_train = zip(*sorted(history.history.items()))
print(zip(*sorted(history.history.items())))
plt.plot(x_train,y_train)
plt.show()

#plt.DataFrame(history.history).plot(figsize=(8,5))
# plt.plot(history.history['acc'])
# plt.plot(history.history['val_acc'])
# plt.title('model accuracy')
# plt.ylabel('accuracy')
# plt.xlabel('epoch')
# plt.legend(['train', 'val'], loc='upper left')
#plt.show()
